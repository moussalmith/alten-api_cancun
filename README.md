# Moussa Ndiaye  
Linkedin: https://www.linkedin.com/in/moussa-ndiaye-851b48125/
E-mail: moussalmith@gmail.com 
## Booking API cancun


## Tech

Api uses a number of open source projects to work properly:

- [Spring Boot](https://start.spring.io/) - HTML enhanced for web apps!
- [Intellij IDEA Editor](https://www.jetbrains.com/fr-fr/idea/) - IntelliJ IDEA is an integrated development environment (IDE) written in Java for developing computer software.
- [Mockito framework](https://site.mockito.org/) - Tasty mocking framework for unit tests in Java ..
- [JUnit 5](https://junit.org/junit5/docs/current/user-guide/) Tasty Mocking for Classes Java
- [Swagger](https://swagger.io/)- Tools to test an Api
- [Jwt](https://jwt.io/) - Json Web Token
- [H2 Database](https://mvnrepository.com/artifact/com.h2database/h2) : Embedded Database 

And of course Dillinger itself is open source with a public https://gitlab.com/moussalmith/alten-api_cancun on Gitlab.

## Installation

Download and install : 
- Maven requires [Maven](https://maven.apache.org/install.html) v3.6.3+ to run.
- JDK 11 requires [JDK11](https://www.oracle.com/fr/java/technologies/javase-jdk11-downloads.html)
- 
## Execution
```sh
git clone https://gitlab.com/moussalmith/alten-api_cancun.git
cd ./alten-api_cancun
mvn spring-boot:run
```

## Access Data in H2 Console

```sh
URL to access H2 console http://localhost:8585/console/login.do
- JDBC URL : jdbc:h2:mem:booking-api
- User Name : cancun
- Password: alten@2021
```

## Testing API
- NB
```sh
I have create ressources for all object to put data in the database in the main classe (CommandLineRunne() method) :
For example a user : 
 username: papis@gmail.com password: alten@2021

```
 - Test with Swagger

```sh
URL to access in Swagger UI
- http://localhost:8585/swagger-ui.html#/
```

- Test with Maven 

```sh
mvn clean test 
```

## Gitlab CI-CD



```sh
https://gitlab.com/moussalmith/alten-api_cancun/-/pipelines/281606220

stages:
  - build - ok
  - test - ok
  - quality - ok
  Results of quality: https://gitlab.com/moussalmith/alten-api_cancun/-/jobs/1154024062/artifacts/browse)

```
Running the application via docker container

```sh

Refer to https://docs.docker.com/get-started/ for details.


```
## License

MIT

