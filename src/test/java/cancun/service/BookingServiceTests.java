package cancun.service;

import cancun.domain.AppRole;
import cancun.domain.Booking;
import cancun.domain.Customer;
import cancun.domain.Room;
import cancun.repository.BookingRepository;
import cancun.service.implement.BookingServiceImpl;
import cancun.web.errors.BadRequestAlertException;
import javassist.tools.rmi.ObjectNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssumptions.given;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingServiceTests {

    @Mock
    private BookingRepository bookingRepository;

    @InjectMocks
    private BookingServiceImpl bookingService ;

    @Mock
            private BookingService bookingServiceI ;


    LocalDate localDate = LocalDate.of(2021, 12, 31);
    Instant timestamp = Instant.now();

    /**
     * Load the authentication context before execute any Test
     */
    @Before
    public void setUp() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        Mockito.when(authentication.getName()).thenReturn("moussalmith@gmail.com");
    }

    /**
     * Testing the save booking
     * @throws ObjectNotFoundException
     */
    @Test
    public void whenSavebooking_shouldReturnBooking() throws ObjectNotFoundException {

        Booking booking = new Booking();
        Customer customer = new Customer();

        Room room = new Room();
        room.setRoomNumber("11");
        room.setRoomCallNumber("08669");
        room.setBooked(false);

        booking.setDuration(1);
        booking.setBookingDate(localDate);
        booking.setBookingDateStart(timestamp);
        booking.setBookingDateEnd(timestamp.plus(1, ChronoUnit.DAYS));
        booking.setCustomer(customer);
        booking.setRoom(room);

        when(bookingRepository.save(ArgumentMatchers.any(Booking.class))).thenReturn(booking);

        Booking createdBooking = bookingService.save(booking);
        // Verifie la duree , les date est louee

        assertThat(createdBooking.getIdBooking()).isSameAs(booking.getIdBooking());

        verify(bookingRepository).save(booking);
    }

    /**
     * Test to return all bookings
     */
    @Test
    public void shouldReturnAllBookings() {
        List<Booking> bookList = new ArrayList<>();
        bookList.add(new Booking());
        when(bookingRepository.findAll()).thenReturn(bookList);
        assertThat(bookList).hasSize(1);
    }

    /**
     * Test to update a booking
     * @throws ObjectNotFoundException
     */
    @Test
    public void shouldUpdatebooking() throws ObjectNotFoundException {

        Booking booking = new Booking();
        booking.setBookingDate(localDate);
        booking.setBookingDateStart(timestamp);
        booking.setBookingDateEnd(timestamp.plus(1, ChronoUnit.DAYS));
        booking.setIdBooking("id1");

        when(bookingRepository.findById("id1")).thenReturn(Optional.of(booking));
        when(bookingRepository.save(booking)).thenReturn(booking);

        bookingService.save(booking);

        booking.setDuration(5);

        verify(bookingRepository, times(1)).save(booking);
        assertThat(booking.getDuration()).isEqualTo(5);
    }

    /**
     * Test Delete a booking if is found
     */
    @Test
    public void shouldDeleteBooking_ifFound() {
        Booking booking = new Booking();
        booking.setIdBooking("12-uud");
        when(bookingRepository.findById(booking.getIdBooking())).thenReturn(Optional.of(booking));
        bookingRepository.deleteById(booking.getIdBooking());
        verify(bookingRepository).deleteById(booking.getIdBooking());
    }

    /**
     * Test the delete a booking when is not exist
     */
    @Test
    public void shouldThrowException_when_booking_does_not_exist() {
        Booking booking = new Booking();
        booking.setIdBooking("uuv-qdd");
        given(bookingRepository.findById(anyString())).hasSameClassAs(Optional.empty());
        bookingService.delete(booking.getIdBooking());
    }


    /**
     * Throw an exception when a another customer book a that is already occupied
     * @throws ObjectNotFoundException
     */
    @Test
    public void shouldThrowException_when_room_is_booked() throws ObjectNotFoundException {
        Room room = new Room();
        room.setRoomNumber("11");
        room.setRoomCallNumber("08669");
        room.setBooked(true);

        Booking booking1 = new Booking();
        booking1.setBookingDate(localDate);
        booking1.setBookingDateStart(timestamp);
        booking1.setBookingDateEnd(timestamp.plus(1, ChronoUnit.DAYS));
        booking1.setIdBooking("id1");
        booking1.setRoom(room);

        assertThat(booking1.getRoom().getBooked()).isTrue();

        Booking booking2 = new Booking();
        booking2.setBookingDate(localDate);
        booking2.setBookingDateStart(timestamp);
        booking2.setBookingDateEnd(timestamp.plus(1, ChronoUnit.DAYS));
        booking2.setIdBooking("id1");
        booking2.setRoom(room);

        doThrow(new BadRequestAlertException(" This room is been booked", "", booking2.getRoom().getRoomNumber()))
        .when(bookingServiceI).save(booking2);
    }

    /**
     * should ThrowException when bookingDateStart is greater than bookingDateStart
     * @throws ObjectNotFoundException
     */
    @Test
    public void shouldThrowException_with_probleme_date() throws ObjectNotFoundException {
        Room room = new Room();
        room.setRoomNumber("11");
        room.setRoomCallNumber("08669");
        room.setBooked(true);

        Booking booking1 = new Booking();
        booking1.setBookingDate(localDate);
        booking1.setBookingDateStart(timestamp.plus(1, ChronoUnit.DAYS));
        booking1.setBookingDateEnd(timestamp);
        booking1.setIdBooking("id1");
        booking1.setRoom(room);

        assertThat(booking1.getRoom().getBooked()).isTrue();

        doThrow(new BadRequestAlertException("The date T2 must be greater than  that of T1","",""))
                .when(bookingServiceI).save(booking1);
    }

    /**
     * should ThrowException when the duration exceed 10 days
     * @throws ObjectNotFoundException
     */
    @Test
    public void shouldThrowException_when_duration_exceed() throws ObjectNotFoundException {
        Room room = new Room();
        room.setRoomNumber("11");
        room.setRoomCallNumber("08669");
        room.setBooked(true);

        Booking booking1 = new Booking();
        booking1.setBookingDate(localDate);
        booking1.setBookingDateStart(timestamp.plus(11, ChronoUnit.DAYS));
        booking1.setBookingDateEnd(timestamp);
        booking1.setIdBooking("id1");
        booking1.setRoom(room);

        assertThat(booking1.getRoom().getBooked()).isTrue();

        doThrow(new BadRequestAlertException(" The number of days booked can not exceed ten (10) days", "", ""))
                .when(bookingServiceI).save(booking1);
    }
}
