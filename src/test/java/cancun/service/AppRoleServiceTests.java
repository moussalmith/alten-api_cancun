package cancun.service;

import cancun.domain.AppRole;
import cancun.repository.AppRoleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssumptions.given;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AppRoleServiceTests {

    @Mock
    private AppRoleRepository appRoleRepository;


    @Test
    public void whenSaveappRole_shouldReturnAppRole() {
        AppRole appRole = new AppRole();
        appRole.setIdRole("cancun@gmail.com");
        appRole.setRoleName("USER");

        when(appRoleRepository.save(ArgumentMatchers.any(AppRole.class))).thenReturn(appRole);

        AppRole createdappRole = appRoleRepository.save(appRole);

        assertThat(createdappRole.getRoleName()).isSameAs(appRole.getRoleName());

        verify(appRoleRepository).save(appRole);
    }

    @Test
    public void shouldReturnAllAppRoles() {
        List<AppRole> appRoleList = new ArrayList<>();
        appRoleList.add(new AppRole());
        when(appRoleRepository.findAll()).thenReturn(appRoleList);
        assertThat(appRoleList).hasSize(1);
    }

    @Test
    public void shouldUpdate_appRole() {
        AppRole appRole = new AppRole();

        appRole.setIdRole("id1");
        appRole.setRoleName("CUSTOMER");
        when(appRoleRepository.findById("id1")).thenReturn(Optional.of(appRole));
        when(appRoleRepository.save(appRole)).thenReturn(appRole);
        appRole.setRoleName("EMPLOYER");
        appRoleRepository.save(appRole);
        verify(appRoleRepository, times(1)).save(appRole);
        assertThat(appRole.getRoleName()).isEqualTo("EMPLOYER");
    }

    @Test
    public void shouldDeleteappRole_ifFound() {
        AppRole appRole = new AppRole();
        appRole.setIdRole("12-uud");
        when(appRoleRepository.findById(appRole.getIdRole())).thenReturn(Optional.of(appRole));
        appRoleRepository.deleteById(appRole.getIdRole());
        verify(appRoleRepository).deleteById(appRole.getIdRole());
    }

    @Test
    public void shouldThrowException_when_appRole_doesnot_exist() {
        AppRole appRole = new AppRole();
        appRole.setIdRole("uuv-qdd");
        given(appRoleRepository.findById(anyString())).hasSameClassAs(Optional.ofNullable(null));
        appRoleRepository.deleteById(appRole.getIdRole());
    }

}
