package cancun.service;

import cancun.domain.Room;
import cancun.repository.RoomRepository;
import cancun.service.implement.RoomServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssumptions.given;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RoomServiceTests {

    @Mock
    private RoomRepository roomRepository;

    @InjectMocks
    private RoomServiceImpl roomService;

    @Test
    public void whenSaveroom_shouldReturnRoom() {
        Room room = new Room();
        room.setBooked(true);
        room.setRoomCallNumber("1234");
        room.setRoomNumber("12");

        when(roomRepository.save(ArgumentMatchers.any(Room.class))).thenReturn(room);

        Room createdroom = roomService.save(room);

        assertThat(createdroom.getIdRoom()).isSameAs(room.getIdRoom());

        verify(roomRepository).save(room);
    }

    @Test
    public void shouldReturnAllRooms() {
        List<Room> roomList = new ArrayList<>();
        roomList.add(new Room());
        when(roomRepository.findAll()).thenReturn(roomList);
        assertThat(roomList).hasSize(1);
    }

    @Test
    public void shouldUpdateroom() {
        Room room = new Room();

        room.setIdRoom("id1");
        room.setBooked(true);
        when(roomRepository.findById("id1")).thenReturn(Optional.of(room));
        when(roomRepository.save(room)).thenReturn(room);
        room.setBooked(false);
        roomService.save(room);
        verify(roomRepository, times(1)).save(room);
        assertThat(room.getBooked()).isEqualTo(false);
    }

    @Test
    public void shouldDeleteroom_ifFound() {
        Room room = new Room();
        room.setIdRoom("12-uud");
        when(roomRepository.findById(room.getIdRoom())).thenReturn(Optional.of(room));
        roomService.delete(room.getIdRoom());
        verify(roomRepository).deleteById(room.getIdRoom());

    }

    @Test
    public void shouldThrowException_when_room_doesnot_exist() {
        Room room = new Room();
        room.setIdRoom("uuv-qdd");
        given(roomRepository.findById(anyString())).hasSameClassAs(Optional.ofNullable(null));
        roomService.delete(room.getIdRoom());
    }
}
