package cancun.service;

import cancun.domain.Hotel;
import cancun.repository.HotelRepository;
import cancun.service.implement.HotelServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssumptions.given;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class HotelServiceTests {

    @Mock
    private HotelRepository hotelRepository;

    @InjectMocks
    private HotelServiceImpl hotelService;

    @Test
    public void whenSaveHotel_shouldReturnUser() {
        Hotel hotel = new Hotel();
        hotel.setCity("Paris");
        hotel.setEmailHotel("cancun@gmail.com");
        hotel.setHotelAdress("4 rue Degaul");
        hotel.setHotelCode(44);
        hotel.setPhoneNumber("08 98 44 33 22");
        hotel.setZipCode("75009");

        when(hotelRepository.save(ArgumentMatchers.any(Hotel.class))).thenReturn(hotel);

        Hotel createdHotel = hotelService.save(hotel);

        assertThat(createdHotel.getEmailHotel()).isSameAs(hotel.getEmailHotel());

        verify(hotelRepository).save(hotel);
    }

    @Test
    public void shouldReturnAllUsers() {
        List<Hotel> hotelList = new ArrayList<>();
        hotelList.add(new Hotel());
        when(hotelRepository.findAll()).thenReturn(hotelList);
        assertThat(hotelList).hasSize(1);
    }

    @Test
    public void shouldUpdateHotel() {
        Hotel hotel = new Hotel();

        hotel.setIdHotel("id1");
        hotel.setCity("PARIS");
        when(hotelRepository.findById("id1")).thenReturn(Optional.of(hotel));
        when(hotelRepository.save(hotel)).thenReturn(hotel);
        hotel.setCity("RENNES");
        hotelService.save(hotel);
        verify(hotelRepository, times(1)).save(hotel);
        assertThat(hotel.getCity()).isEqualTo("RENNES");
    }

    @Test
    public void shouldDeleteHotel_ifFound() {
        Hotel hotel = new Hotel();
        hotel.setIdHotel("12-uud");
        when(hotelRepository.findById(hotel.getIdHotel())).thenReturn(Optional.of(hotel));
        hotelService.delete(hotel.getIdHotel());
        verify(hotelRepository).deleteById(hotel.getIdHotel());

    }

    @Test
    public void shouldThrowException_when_hotel_doesnot_exist() {
        Hotel hotel = new Hotel();
        hotel.setIdHotel("uuv-qdd");
        given(hotelRepository.findById(anyString())).hasSameClassAs(Optional.ofNullable(null));
        hotelService.delete(hotel.getIdHotel());
    }

}
