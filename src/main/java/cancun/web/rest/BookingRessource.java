package cancun.web.rest;

import cancun.domain.Booking;
import cancun.service.BookingService;
import cancun.web.errors.BadRequestAlertException;
import cancun.web.utils.HeaderUtil;
import cancun.web.utils.PaginationUtil;
import cancun.web.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.tools.rmi.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST Controller for mapping {@link Booking}
 */
@RestController
@RequestMapping("/api")
@Api(value = "booking")
public class BookingRessource {

    private final Logger log = LoggerFactory.getLogger(BookingRessource.class);

    private static final String ENTITY_NAME = "api-cancun";

    @Value("${spring.application.name}")
    private String applicationName;

    private final BookingService bookingService ;

    public BookingRessource(BookingService bookingService1) {
        this.bookingService = bookingService1;
    }

    /**
     * {@code POST  /booking} : Create a new booking.
     *
     * @param booking the Booking to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new booking, or with status {@code 400 (Bad Request)} if the booking has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/booking")
    public ResponseEntity<Booking> createBooking(@Valid @RequestBody Booking booking) throws URISyntaxException, ObjectNotFoundException {
        log.debug("REST request to save Booking : {}", booking);
        long duration = bookingService.getNumberDay(booking.getBookingDateStart(), booking.getBookingDateEnd());
        booking.setDuration(duration);
        if (booking.getIdBooking() != null) {
            throw new ObjectNotFoundException("Reservation is not possible with the id " +booking.getIdBooking());
        }
        else if (booking.getRoom().getBooked()) {
            throw new BadRequestAlertException(" This room is been booked", "", booking.getRoom().getRoomNumber());
        }
        Booking result = bookingService.save(booking);
        return ResponseEntity.created(new URI("/api/booking/" + result.getIdBooking()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getIdBooking()))
                .body(result);
    }

    /**
     * {@code PUT  /booking} : Updates an existing booking.
     *
     * @param booking the booking to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated booking,
     * or with status {@code 400 (Bad Request)} if the booking is not valid,
     * or with status {@code 500 (Internal Server Error)} if the booking couldn't be updated.
     */
    @PutMapping("/booking")
    public ResponseEntity<Booking> updateBooking(@Valid @RequestBody Booking booking) throws ObjectNotFoundException {
        log.debug("REST request to update Booking : {}", booking);
        if (booking.getIdBooking() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        long duration = bookingService.getNumberDay(booking.getBookingDateStart(), booking.getBookingDateEnd());
        booking.setDuration(duration);
        Booking result = bookingService.save(booking);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, booking.getIdBooking()))
                .body(result);
    }

    /**
     * {@code GET  /booking} : get all the bookings.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bookings in body.
     */
    @ApiOperation(value = "View a list of available Bookings", response = Booking.class)
    @GetMapping("/bookings")
    public ResponseEntity<List<Booking>> getAllBookings(Pageable pageable) {
        log.debug("REST request to get a page of Bookings");
        Page<Booking> page = bookingService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /bookings/:idBooking} : get the "id" booking.
     *
     * @param idBooking the id of the booking to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the booking, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/booking/{idBooking}")
    public ResponseEntity<Booking> getBooking(@PathVariable String idBooking) {
        log.debug("REST request to get Booking : {}", idBooking);
        Optional<Booking> booking = bookingService.findOne(idBooking);
        return ResponseUtil.wrapOrNotFound(booking);
    }

    /**
     * {@code DELETE  /booking/:idBooking} : delete the "id" booking.
     *
     * @param idBooking the id of the booking to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/booking/{idBooking}")
    public ResponseEntity<Void> deleteBooking(@PathVariable String idBooking) {
        log.debug("REST request to delete Booking : {}", idBooking);
        bookingService.delete(idBooking);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, idBooking)).build();
    }

    /**
     * {@code SEARCH  /_search/bookings?query=:query} : search for the booking corresponding
     * to the query.
     *
     * @param query    the query of the booking search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/bookings")
    public ResponseEntity<List<Booking>> searchBookings(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Bookings for query {}", query);
        Page<Booking> page = bookingService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
