package cancun.web.rest;

import cancun.domain.Room;
import cancun.service.RoomService;
import cancun.web.errors.BadRequestAlertException;
import cancun.web.utils.HeaderUtil;
import cancun.web.utils.PaginationUtil;
import cancun.web.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST Controller for mapping {@link Room}
 */
@RestController
@RequestMapping("/api")
@Api(value = "room")
public class RoomRessource {

    private final Logger log = LoggerFactory.getLogger(RoomRessource.class);

    private static final String ENTITY_NAME = "api-cancun";

    @Value("${spring.application.name}")
    private String applicationName;

    private final RoomService roomService;

    public RoomRessource(RoomService roomService) {
        this.roomService = roomService;
    }

    /**
     * {@code POST  /room} : Create a new room.
     *
     * @param room the room to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new room, or with status {@code 400 (Bad Request)} if the room has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/room")
    public ResponseEntity<Room> createRoom(@Valid @RequestBody Room room) throws URISyntaxException {
        log.debug("REST request to save room : {}", room);
        if (room.getIdRoom() != null) {
            throw new BadRequestAlertException("A new room cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Room result = roomService.save(room);
        return ResponseEntity.created(new URI("/api/room/" + result.getIdRoom()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getIdRoom()))
                .body(result);
    }

    /**
     * {@code PUT  /Room} : Updates an existing Room.
     *
     * @param room the Room to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated Room,
     * or with status {@code 400 (Bad Request)} if the Room is not valid,
     * or with status {@code 500 (Internal Server Error)} if the Room couldn't be updated.
     */
    @PutMapping("/Room")
    public ResponseEntity<Room> updateRoom(@Valid @RequestBody Room room) {
        log.debug("REST request to update Room : {}", room);
        if (room.getIdRoom() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Room result = roomService.save(room);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, room.getIdRoom()))
                .body(result);
    }

    /**
     * {@code GET  /Room} : get all the Rooms.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of Rooms in body.
     */
    @ApiOperation(value = "View a list of available Rooms", response = Room.class)
    @GetMapping("/rooms")
    public ResponseEntity<List<Room>> getAllRooms(Pageable pageable) {
        log.debug("REST request to get a page of Rooms");
        Page<Room> page = roomService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /room/:idRoom} : get the "id" Room.
     *
     * @param idRoom the id of the Room to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the Room, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/room/{idRoom}")
    public ResponseEntity<Room> getRoom(@PathVariable String idRoom) {
        log.debug("REST request to get Room : {}", idRoom);
        Optional<Room> room = roomService.findOne(idRoom);
        return ResponseUtil.wrapOrNotFound(room);
    }

    /**
     * {@code DELETE  /room/:idRoom} : delete the "id" Room.
     *
     * @param idRoom the id of the Room to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/room/{idRoom}")
    public ResponseEntity<Void> deleteRoom(@PathVariable String idRoom) {
        log.debug("REST request to delete Room : {}", idRoom);
        roomService.delete(idRoom);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, idRoom)).build();
    }

    /**
     * {@code SEARCH  /_search/rooms?query=:query} : search for the Room corresponding
     * to the query.
     *
     * @param query    the query of the Room search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/rooms")
    public ResponseEntity<List<Room>> searchRooms(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Rooms for query {}", query);
        Page<Room> page = roomService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
