package cancun.web.rest;

import cancun.domain.Hotel;
import cancun.service.HotelService;
import cancun.web.errors.BadRequestAlertException;
import cancun.web.utils.HeaderUtil;
import cancun.web.utils.PaginationUtil;
import cancun.web.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST Controller for mapping {@link Hotel}
 */
@RestController
@RequestMapping("/api")
@Api(value = "hotel")
public class HotelRessource {

    private final Logger log = LoggerFactory.getLogger(HotelRessource.class);

    private static final String ENTITY_NAME = "api-cancun";

    @Value("${spring.application.name}")
    private String applicationName;

    private final HotelService hotelService ;

    public HotelRessource(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    /**
     * {@code POST  /hotel} : Create a new hotel.
     *
     * @param hotel the hotel to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hotel, or with status {@code 400 (Bad Request)} if the hotel has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hotel")
    public ResponseEntity<Hotel> createHotel(@Valid @RequestBody Hotel hotel) throws URISyntaxException {
        log.debug("REST request to save Hotel : {}", hotel);
        if (hotel.getIdHotel() != null) {
            throw new BadRequestAlertException("A new Hotel cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Hotel result = hotelService.save(hotel);
        return ResponseEntity.created(new URI("/api/hotel/" + result.getIdHotel()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getIdHotel()))
                .body(result);
    }

    /**
     * {@code PUT  /hotel} : Updates an existing hotel.
     *
     * @param hotel the hotel to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hotel,
     * or with status {@code 400 (Bad Request)} if the hotel is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hotel couldn't be updated.
     */
    @PutMapping("/hotel")
    public ResponseEntity<Hotel> updateHotel(@Valid @RequestBody Hotel hotel) {
        log.debug("REST request to update Hotel : {}", hotel);
        if (hotel.getIdHotel() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Hotel result = hotelService.save(hotel);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hotel.getIdHotel()))
                .body(result);
    }

    /**
     * {@code GET  /hotel} : get all the hotels.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hotels in body.
     */
    @ApiOperation(value = "View a list of available Hotels", response = Hotel.class)
    @GetMapping("/hotels")
    public ResponseEntity<List<Hotel>> getAllHotels(Pageable pageable) {
        log.debug("REST request to get a page of Hotels");
        Page<Hotel> page = hotelService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /hotels/:idHotel} : get the "id" hotel.
     *
     * @param idHotel the id of the hotel to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hotel, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hotel/{idHotel}")
    public ResponseEntity<Hotel> getHotel(@PathVariable String idHotel) {
        log.debug("REST request to get Hotel : {}", idHotel);
        Optional<Hotel> hotel = hotelService.findOne(idHotel);
        return ResponseUtil.wrapOrNotFound(hotel);
    }

    /**
     * {@code DELETE  /hotel/:idHotel} : delete the "id" hotel.
     *
     * @param idHotel the id of the hotel to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hotel/{idHotel}")
    public ResponseEntity<Void> deleteHotel(@PathVariable String idHotel) {
        log.debug("REST request to delete Hotel : {}", idHotel);
        hotelService.delete(idHotel);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, idHotel)).build();
    }

    /**
     * {@code SEARCH  /_search/hotels?query=:query} : search for the hotel corresponding
     * to the query.
     *
     * @param query    the query of the hotel search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/hotels")
    public ResponseEntity<List<Hotel>> searchHotels(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Hotels for query {}", query);
        Page<Hotel> page = hotelService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
