package cancun.web.rest;

import cancun.domain.AppUser;
import cancun.domain.Customer;
import cancun.service.AppUserService;
import cancun.web.errors.BadRequestAlertException;
import cancun.web.utils.HeaderUtil;
import cancun.web.utils.PaginationUtil;
import cancun.web.utils.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST Controller for mapping {@link Customer
 * }
 */
@RestController
@RequestMapping("/api")
@Api(value = "customer")
public class CustomerRessource {

    private final Logger log = LoggerFactory.getLogger(CustomerRessource.class);

    private static final String ENTITY_NAME = "api-cancun";

    @Value("${spring.application.name}")
    private String applicationName;

    private final AppUserService appUserService;

    public CustomerRessource(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    /**
     * {@code POST  /customer} : Create a new customer.
     *
     * @param customer the customer to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customer, or with status {@code 400 (Bad Request)} if the Customer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/customer")
    public ResponseEntity<Customer> createCustomer(@Valid @RequestBody Customer customer) throws URISyntaxException {
        log.debug("REST request to save customer: {}", customer);
        if (customer.getUsername() != null) {
            throw new BadRequestAlertException("A new customer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Customer result = appUserService.save(customer);
        return ResponseEntity.created(new URI("/api/customer/" + result.getUsername()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getUsername()))
                .body(result);
    }

    /**
     * {@code PUT  /Customer} : Updates an existing Customer.
     *
     * @param customer the Customer to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated Customer,
     * or with status {@code 400 (Bad Request)} if the Customer is not valid,
     * or with status {@code 500 (Internal Server Error)} if the Customer couldn't be updated.
     */
    @PutMapping("/customer")
    public ResponseEntity<Customer> updateCustomer(@Valid @RequestBody Customer customer) {
        log.debug("REST request to update Customer:{} ", customer);
        if (customer.getUsername() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Customer result = appUserService.save(customer);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, customer.getUsername()))
                .body(result);
    }

    /**
     * {@code GET  /Customer} : get all the Customers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of Customers in body.
     */
    @ApiOperation(value = "View a list of available Customers", response = Customer.
            class)
    @GetMapping("/customers")
    public ResponseEntity<List<AppUser>> getAllCustomers(Pageable pageable) {
        log.debug("REST request to get a page of Customers");
        Page<AppUser> page = appUserService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /customer/:idCustomer} : get the "id" Customer.
     *
     * @param idCustomer the id of the Customer to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the Customer or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/customer/ {idCustomer }")
    public ResponseEntity<AppUser> getCustomer(@PathVariable String idCustomer) {
        log.debug("REST request to get Customer:{} ", idCustomer);
        Optional<AppUser> customer = appUserService.findOne(idCustomer);
        return ResponseUtil.wrapOrNotFound(customer);
    }

    /**
     * {@code DELETE  /customer/:idCustomer} : delete the "id" Customer.
     *
     * @param idCustomer the id of the Customer to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/customer/ {idCustomer}") public ResponseEntity<Void>deleteCustomer(@PathVariable String idCustomer) {
        log.debug("REST request to delete Customer:{} ", idCustomer);
        appUserService.delete(idCustomer);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, idCustomer))
                .build();
    }

    /**
     * {@code SEARCH  /_search/customers?query=:query} : search for the Customer corresponding to the query.
     *
     * @param query    the query of the Customer search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/customers")
    public ResponseEntity<List<Customer>> searchCustomers(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of Customer s for query {} ", query);
        Page<Customer> page = appUserService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
