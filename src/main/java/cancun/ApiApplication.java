package cancun;

import cancun.domain.*;
import cancun.repository.AppRoleRepository;
import cancun.repository.AppUserRepository;
import cancun.repository.BookingRepository;
import cancun.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Role;

import java.awt.print.Book;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

@SpringBootApplication
public class ApiApplication {


    private LocalDate localDate1  =   LocalDate.of(2021, 4, 2);

    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }

    @Bean
    CommandLineRunner start(BookingRepository bookingRepository, BookingService bookingService, AppRoleService appRoleService, HotelService hotelService, AccountService accountService, RoomService roomService, AppUserService appUserService) {
        return args -> {
            Hotel cancun = hotelService.save(new Hotel( 12, " 6 Rue Cancun", "13423",
                    "Montreal", "+3305886886865", "cancun@cancun.org"));

           Stream.of("CUSTOMER", "EMPLOYE", "USER")
                    .forEach(one -> {
                        appRoleService.save(new AppRole(one));
                    });


           Stream.of( "papis@gmail.com", "jean@gmail.com","pierre@gmail.com", "edouard@gmail.com","eric@gmail.com", "dupont@gmail.com")
                    .forEach(one -> {
                        accountService.saveUser(one, "alten@2021", "alten@2021");
                    });



            Set<AppRole> appRoles =new HashSet<>(0);
            //appRoles.add((AppRole) appRoleRepository.findAll());


            AppUser employer =  appUserService.save(new Employer("laye@gmail.com","passer", true, "Abdoulaye", "diop"));

            AppRole appRole = appRoleService.findByRoleName("CUSTOMER");


            employer.getAppUserRole().add(appRole);

            Room room1 = roomService.save(new Room("07 55 43 55 43 ", "2132", true, cancun));
           //  System.out.println(" Customer  ----     ----------     --------  "+customer1.getUsername());
           // Booking booking1 = bookingService.save(new Booking(LocalDate.of(2021, 04, 01), Instant.now(),
                    //Instant.now(), employer, room1));
           Booking booking2 = bookingRepository.save(new Booking(LocalDate.of(2022, 04, 01), Instant.now(),
                    Instant.now() ));
            LocalDate localDate = LocalDate.of(2015, 12, 31);
            Instant timestamp = Instant.now();

          //  System.out.println(" All Booking  " + bookingRepository.save(new Booking(localDate, timestamp, timestamp, customer1)));
            System.out.println(bookingRepository.findAll().stream().findFirst());
        };
    }
}
