package cancun.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Set;

@Entity
@DiscriminatorValue("CUSTOMER")
public class Customer extends AppUser {

    @NotNull
    private String adress ;

    @NotNull
    private String zipCode ;

    @NotNull
    private String phoneNumber ;

    private FunctionType functionType = FunctionType.CUSTOMER ;



    public Customer(String username, String password, Boolean activated, Set<AppRole> appUserRole, @NotNull String adress, @NotNull String zipCode, @NotNull String phoneNumber ) {
        super(username, password, activated, appUserRole);
        this.adress = adress;
        this.zipCode = zipCode;
        this.phoneNumber = phoneNumber;
    }

    public Customer() {
        super();
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

/*    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }*/

    public FunctionType getFunctionType() {
        return functionType;
    }

    public void setFunctionType(FunctionType functionType) {
        this.functionType = functionType;
    }
}
