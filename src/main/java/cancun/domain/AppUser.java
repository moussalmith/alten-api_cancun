package cancun.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_user", discriminatorType = DiscriminatorType.STRING)
@JsonDeserialize(as = Customer.class)
public abstract class AppUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String idUser;
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private Boolean activated ;
    @NotNull
    private String name ;
    @NotNull
    private  String lastName ;
    @OneToOne(mappedBy = "appUser", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    @JsonIgnore
    private Booking booking;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_ide")
    )
    private Set<AppRole> appUserRole  = new HashSet<AppRole>(0);

    public AppUser(String username, String password, Boolean activated, Set<AppRole> appUserRole) {
        this.username = username;
        this.password = password;
        this.activated = activated;
        this.appUserRole = appUserRole;
    }

    public AppUser(String username, String password, Boolean activated, @NotNull String name, @NotNull String lastName) {
        this.username = username;
        this.password = password;
        this.activated = activated;
        this.name = name;
        this.lastName = lastName;
    }

    public AppUser() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Set<AppRole> getAppUserRole() {
        return appUserRole;
    }

    public void setAppUserRole(Set<AppRole> appUserRole) {
        this.appUserRole = appUserRole;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }
}
