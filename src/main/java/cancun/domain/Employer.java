package cancun.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Set;

@Entity
@DiscriminatorValue("EMPLOYE")
public class Employer extends AppUser{

    private  FunctionType functionType = FunctionType.EMPLOYE ;
    public Employer(String username, String password, Boolean activated, Set<AppRole> appUserRole) {
        super(username, password, activated, appUserRole);
    }

    public Employer(String username, String password, Boolean activated, @NotNull String name, @NotNull String lastName ) {
        super(username, password, activated, name, lastName);
    }

    public Employer() {
    }

    public FunctionType getFunctionType() {
        return functionType;
    }

    public void setFunctionType(FunctionType functionType) {
        this.functionType = functionType;
    }
}
