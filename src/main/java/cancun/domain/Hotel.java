package cancun.domain;



import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "hotel_infos")
public class Hotel implements Serializable {

    private static final  long serialVersionUID = 1L ;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private  String idHotel;

    @NotNull
    private int hotelCode ;

    @NotNull
    private String hotelAdress ;

    @NotNull
    private String zipCode ;

    @NotNull
    private String  city ;

    @NotNull
    @Email
    private String emailHotel ;

    @NotNull
    private String phoneNumber ;

    @JsonIgnore
    @OneToMany(targetEntity = Room.class, mappedBy = "hotel")
    private Set<Room> rooms = new HashSet<>();

    public Hotel() {}

    public Hotel(int hotelCode, String hotelAdress, String zipCode, String city, String phoneNumber, String emailHotel) {
        this.hotelCode = hotelCode;
        this.hotelAdress = hotelAdress;
        this.zipCode = zipCode;
        this.city = city;
        this.phoneNumber = phoneNumber;
        this.emailHotel = emailHotel;
    }

    public Hotel(String idHotel, @NotNull int hotelCode, @NotNull String hotelAdress, @NotNull String zipCode, @NotNull String city,String phoneNumber, @NotNull @Email String emailHotel) {
        this.idHotel = idHotel;
        this.hotelCode = hotelCode;
        this.hotelAdress = hotelAdress;
        this.zipCode = zipCode;
        this.city = city;
        this.phoneNumber = phoneNumber ;
        this.emailHotel = emailHotel;
    }

    public String getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(String id) {
        this.idHotel = id;
    }

    public int getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(int hotelCode) {
        this.hotelCode = hotelCode;
    }

    public String getHotelAdress() {
        return hotelAdress;
    }

    public void setHotelAdress(String hotelAdress) {
        this.hotelAdress = hotelAdress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Set<Room> getRooms() {
        return rooms;
    }

    public void setRooms(Set<Room> rooms) {
        this.rooms = rooms;
    }

    public String getEmailHotel() {
        return emailHotel;
    }

    public void setEmailHotel(String emailHotel) {
        this.emailHotel = emailHotel;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
