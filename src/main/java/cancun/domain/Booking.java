package cancun.domain;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

@Entity(name = "booking")
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String idBooking;

    @NotNull
    private LocalDate bookingDate ;

    @NotNull
    private  Instant bookingDateStart ;

    @NotNull
    private Instant bookingDateEnd ;

    private long duration ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private AppUser appUser ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_room")
    private Room room ;

    public Booking() {}

    public Booking(LocalDate bookingDate, Instant bookingDateStart, Instant bookingDateEnd) {
        this.bookingDate = bookingDate;
        this.bookingDateStart = bookingDateStart;
        this.bookingDateEnd = bookingDateEnd;
    }

    public Booking(@NotNull LocalDate bookingDate, @NotNull Instant bookingDateStart, @NotNull Instant bookingDateEnd, @NotNull AppUser appUser, @NotNull Room room) {
        this.bookingDate = bookingDate;
        this.bookingDateStart = bookingDateStart;
        this.bookingDateEnd = bookingDateEnd;
        this.appUser = appUser;
        this.room = room;
    }

    public String getIdBooking() {
        return idBooking;
    }

    public void setIdBooking(String id) {
        this.idBooking = id;
    }

    public LocalDate getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDate bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Instant getBookingDateStart() {
        return bookingDateStart;
    }

    public void setBookingDateStart(Instant bookingDateStart) {
        this.bookingDateStart = bookingDateStart;
    }

    public Instant getBookingDateEnd() {
        return bookingDateEnd;
    }

    public void setBookingDateEnd(Instant bookingDateEnd) {
        this.bookingDateEnd = bookingDateEnd;
    }

    public AppUser getCustomer() {
        return appUser;
    }

    public void setCustomer(AppUser customer) {
        this.appUser = customer;
    }


    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
