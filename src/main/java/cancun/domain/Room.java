package cancun.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Room implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String idRoom;

    @NotNull
    private String roomNumber ;

    @NotNull
    private  Boolean isBooked ;

    @NotNull
    private String roomCallNumber ;

    @ManyToOne
    @JoinColumn(name = "idHotel", nullable = false)
    @JsonIgnore
    private Hotel hotel;

    @OneToOne(mappedBy = "room", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    @JsonIgnore
    private Booking booking;

    public Room() {}

    public Room(String roomNumber, String roomCallNumber, Boolean isBooked, Hotel hotel) {
        this.roomNumber = roomNumber;
        this.roomCallNumber = roomCallNumber;
        this.isBooked =isBooked;
        this.hotel = hotel;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomCallNumber() {
        return roomCallNumber;
    }

    public void setRoomCallNumber(String roomCallNumber) {
        this.roomCallNumber = roomCallNumber;
    }

    public void setIdRoom(String id) {
        this.idRoom = id;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Boolean getBooked() {
        return isBooked;
    }

    public void setBooked(Boolean booked) {
        isBooked = booked;
    }

    public String getIdRoom() {
        return idRoom;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
