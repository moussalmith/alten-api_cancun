package cancun.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class AppRole implements Serializable {

    @Id
    @GeneratedValue(generator="system-uuid", strategy = GenerationType.IDENTITY)
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String idRole;

    private String roleName;

    public AppRole(String roleName) {
        this.roleName = roleName;
    }

    public AppRole() {

    }

    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
