package cancun.security;

import cancun.domain.AppUser;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Service Implementation for managing {@link UsernamePasswordAuthenticationFilter}
 */
public class JWTAuthenticationFilter  extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;
    private final Set<String> roles = new HashSet<>();


    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    /**
     *  Attempt the authentication
     *
     * @param request
     * @param response
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            AppUser appUser = new ObjectMapper().readValue(request.getInputStream(), AppUser.class);
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(appUser.getUsername(), appUser.getPassword());
            return authenticationManager.authenticate(authenticationToken);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     *
     *
     * @param request
     * @param response
     * @param chain
     * @param authResult
     * @throws IOException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) throws IOException {
        String jwtAccessToken = generateToken(request, authResult, SecurityParam.EXPIRATION_ACCESS_TOKEN);
        String jwtRefreshToken = generateToken(request, authResult, SecurityParam.EXPIRATION_REFRESH_TOKEN);
        HashMap<String, String> idToken = new HashMap<>();
        idToken.put(SecurityParam.REFRESH_TOKEN, jwtRefreshToken);
        idToken.put(SecurityParam.ACCESS_TOKEN, jwtAccessToken);
        response.setContentType(SecurityParam.CONTENT_TYPE_JSON);
        new ObjectMapper().writeValue(response.getOutputStream(), idToken);

    }

    /**
     * Methode to generate a token
     *
     * @param request
     * @param authResult
     * @param expiration
     * @return the token
     */
    public String generateToken(HttpServletRequest request, Authentication authResult, long expiration) {
        User user = (User) authResult.getPrincipal();
        for (GrantedAuthority a : authResult.getAuthorities()) {
            roles.add(a.getAuthority());
        }
        return JWT.create()
                .withIssuer(request.getRequestURL().toString())
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + expiration))
                .withArrayClaim(SecurityParam.ROLE_CLAIM, roles.toArray(new String[roles.size()]))
                .sign(Algorithm.HMAC256(SecurityParam.SECRET));
    }
}
