package cancun.security;


import cancun.domain.AppUser;
import cancun.domain.Booking;
import cancun.repository.AppUserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;


/**
 * Service Implementation for managing {@link Booking}
 */
@Service
public class UserDetailServiceImplementation implements UserDetailsService {

    private final AppUserRepository appUserRepository;

    public UserDetailServiceImplementation(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = appUserRepository.findByUsername(username);
        if (appUser == null)throw new UsernameNotFoundException("Invalid User");
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        appUser.getAppUserRole().forEach( r->{
            grantedAuthorities.add(new SimpleGrantedAuthority(r.getRoleName()));
        });
        return new User(appUser.getUsername(), appUser.getPassword(), grantedAuthorities);
    }
}


