package cancun.security;


import cancun.domain.Booking;

/**
 * Parameters for the Security
 */
public interface SecurityParam {
    String JWT_HEADER_NAME = "Authorization";
    String SECRET = "moussalmith@gmail.com";
    String ACCESS_TOKEN = "access_token";
    String REFRESH_TOKEN = "refresh_token";
    String ROLE_CLAIM = "roles";
    String CONTENT_TYPE_JSON = "application/json";
    String ERROR_MESSAGE = "error-message";
    long EXPIRATION_ACCESS_TOKEN = 864000 ;
    long EXPIRATION_REFRESH_TOKEN = 900000; // 15 mn x 60 x 1000
    String HEADER_PREFIX = "Bearer ";
}

