package cancun.repository;

import cancun.domain.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AppRoleRepository extends JpaRepository<AppRole, String> {

    AppRole findByRoleName(String rolename);
}
