package cancun.service;

import cancun.domain.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing  {@link Room}
 */
public interface RoomService {

    /**
     * Save a room.
     *
     * @param room the entity to save.
     * @return the persisted entity.
     */
    Room save(Room room);

    /**
     * Get all the room.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Room> findAll(Pageable pageable);

    /**
     * Get the "idRoom" room.
     *
     * @param idRoom the id of the entity.
     * @return the entity.
     */
    Optional<Room> findOne(String idRoom);

    /**
     * Delete the "idRoom" room.
     *
     * @param idRoom the id of the entity.
     */
    void delete(String idRoom);

    /**
     * Search for the room corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Room> search(String query, Pageable pageable);
}
