package cancun.service.implement;

import cancun.domain.AppUser;
import cancun.domain.Booking;
import cancun.repository.AppUserRepository;
import cancun.repository.BookingRepository;
import cancun.repository.RoomRepository;
import cancun.service.AccountService;
import cancun.service.BookingService;
import cancun.web.errors.BadRequestAlertException;
import javassist.tools.rmi.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Booking}
 */
@Service
public class BookingServiceImpl implements BookingService {

    private final Logger log = LoggerFactory.getLogger(Booking.class);

    private final BookingRepository bookingRepository;

    private final AppUserRepository appUserRepository;

    private final RoomRepository roomRepository;

    private final AccountService accountService;

    public BookingServiceImpl(BookingRepository bookingRepository, AppUserRepository appUserRepository, RoomRepository roomRepository, AccountService accountService) {
        this.bookingRepository = bookingRepository;
        this.appUserRepository = appUserRepository;
        this.roomRepository = roomRepository;
        this.accountService = accountService;
    }

    @Override
    public Booking save(Booking booking) {
        log.debug("Request to save Booking : {}", booking);
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();
        final long numberDay  = getNumberDay(booking.getBookingDateStart(), booking.getBookingDateEnd());
        try {
            AppUser appUser =  accountService.loadUserByUsername(username);
            booking.setCustomer(appUser);
            booking.setDuration(numberDay);
        }
        catch (Exception exception) {
            exception.getMessage();
        }
        return bookingRepository.save(booking);
    }

    @Override
    public Page<Booking> findAll(Pageable pageable) {
        log.debug("Request to get all Booking");
        return bookingRepository.findAll(pageable);
    }

    @Override
    public Optional<Booking> findOne(String idBooking) {
        log.debug("Request to get Booking : {}", idBooking);
        return Optional.empty();
    }

    @Override
    public void delete(String idBooking) {
        log.debug("Request to delete Booking: {}", idBooking);

    }

    @Override
    public Page<Booking> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Booking for query  {}", query);
        return null;
    }

    @Override
    public long getNumberDay(Instant instant1, Instant instant2) {
        log.debug(" Request to verify the booking reservation are met: the duration ");
        long duration =  ChronoUnit.DAYS.between(instant1, instant2);
        final long numberMaxDayAuthorize = 10 ;
        if (instant2.isBefore(instant1)) {
            throw new BadRequestAlertException("The date T2 must be greater than  that of T1","","");
        }
        else if (duration > numberMaxDayAuthorize) {
            throw  new BadRequestAlertException(" The number of days booked can not exceed ten (10) days", "", "");
        }
        return duration;
    }
}
