package cancun.service.implement;

import cancun.domain.Customer;
import cancun.domain.Room;
import cancun.repository.HotelRepository;
import cancun.repository.RoomRepository;
import cancun.service.RoomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoomServiceImpl implements RoomService {

    private final Logger log = LoggerFactory.getLogger(Customer.class);

    private final RoomRepository roomRepository ;

    private final HotelRepository hotelRepository ;

    public RoomServiceImpl(RoomRepository roomRepository, HotelRepository hotelRepository) {
        this.roomRepository = roomRepository;
        this.hotelRepository = hotelRepository;
    }

    @Override
    public Room save(Room room ) {
        log.debug(" Request to save Room : {}", room);
        return roomRepository.save(room);
    }

    @Override
    public Page<Room> findAll(Pageable pageable) {
        log.debug("Request to get all Rooms");
        return roomRepository.findAll(pageable);
    }

    @Override
    public Optional<Room> findOne(String idRoom) {
        log.debug("Request to get Room : {}", idRoom);
        return roomRepository.findById(idRoom);
    }

    @Override
    public void delete(String idRoom) {
        log.debug("Request to delete Room : {}", idRoom);
        roomRepository.deleteById(idRoom);
    }

    @Override
    public Page<Room> search(String query, Pageable pageable) {
        log.debug("Request to search  for a page of Room for query {}", query);
        return null;
    }
}
