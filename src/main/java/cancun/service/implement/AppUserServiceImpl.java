package cancun.service.implement;

import cancun.domain.AppUser;
import cancun.domain.Customer;
import cancun.domain.Employer;
import cancun.repository.AppUserRepository;
import cancun.service.AppUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for mapping {@link Customer}
 */
@Service
public class AppUserServiceImpl implements AppUserService {

    private final Logger log = LoggerFactory.getLogger(Customer.class);

    private final AppUserRepository appUserRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder ;

    public AppUserServiceImpl(AppUserRepository appUserRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.appUserRepository = appUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public Customer save(Customer customer) {
        customer.setPassword(bCryptPasswordEncoder.encode(customer.getPassword()));
        log.debug(" Request to save Customer : {}", customer);
        return appUserRepository.save(customer);
    }

    @Override
    public Employer save(Employer employer) {
        employer.setPassword(bCryptPasswordEncoder.encode(employer.getPassword()));
        log.debug(" Request to save Customer : {}", employer);
        return appUserRepository.save(employer);
    }

    @Override
    public Page<AppUser> findAll(Pageable pageable) {
        log.debug("Request to get all Customers");
        return appUserRepository.findAll(pageable);
    }

    @Override
    public Optional<AppUser> findOne(String idCustomer) {
        log.debug("Request to get Customer : {}", idCustomer);
        return appUserRepository.findById(idCustomer);
    }

    @Override
    public void delete(String idCustomer) {
        log.debug("Request to delete Customer : {}", idCustomer);
        appUserRepository.deleteById(idCustomer);
    }

    @Override
    public Page<Customer> search(String query, Pageable pageable) {
        log.debug("Request to search  for a page of Customer for query {}", query);
        return null;
    }
}
