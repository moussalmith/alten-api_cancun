package cancun.service.implement;

import cancun.domain.AppRole;
import cancun.repository.AppRoleRepository;
import cancun.service.AppRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for mapping {@link AppRole}
 */
@Service
public class AppRoleServiceImpl implements AppRoleService {
    private final Logger log = LoggerFactory.getLogger(AppRole.class);

    private final AppRoleRepository appRoleRepository;

    @Override
    public AppRole findByRoleName(String roleName) {
        return appRoleRepository.findByRoleName(roleName);
    }

    public AppRoleServiceImpl(AppRoleRepository appRoleRepository) {
        this.appRoleRepository = appRoleRepository;
    }


    @Override
    public AppRole save(AppRole appRole) {
        log.debug(" Request to save AppRole : {}", appRole);
        return appRoleRepository.save(appRole);
    }

    @Override
    public Page<AppRole> findAll(Pageable pageable) {
        log.debug("Request to get all AppRoles");
        return appRoleRepository.findAll(pageable);
    }

    @Override
    public Optional<AppRole> findOne(String idAppRole) {
        log.debug("Request to get AppRole : {}", idAppRole);
        return appRoleRepository.findById(idAppRole);
    }

    @Override
    public void delete(String idAppRole) {
        log.debug("Request to delete AppRole : {}", idAppRole);
        appRoleRepository.deleteById(idAppRole);
    }

    @Override
    public Page<AppRole> search(String query, Pageable pageable) {
        log.debug("Request to search  for a page of AppRole for query {}", query);
        return null;
    }
}
