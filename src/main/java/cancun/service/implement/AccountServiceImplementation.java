package cancun.service.implement;

import cancun.domain.AppRole;
import cancun.domain.AppUser;
import cancun.domain.Booking;
import cancun.domain.Customer;
import cancun.repository.AppRoleRepository;
import cancun.repository.AppUserRepository;
import cancun.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


/**
 * Service Implementation for managing {@link AppUser}
 */
@Service
@Transactional
public class AccountServiceImplementation implements AccountService {

    private final Logger log = LoggerFactory.getLogger(Booking.class);

    private final AppUserRepository appUserRepository ;
    private final BCryptPasswordEncoder bCryptPasswordEncoder ;
    private final AppRoleRepository appRoleRepository;
    private static final String DEFAULT_ROLE = "USER";

    public AccountServiceImplementation(AppUserRepository appUserRepository, BCryptPasswordEncoder bCryptPasswordEncoder, AppRoleRepository appRoleRepository) {
        this.appUserRepository = appUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder ;
        this.appRoleRepository = appRoleRepository;
    }

    @Override
    public AppUser saveUser(String email, String password, String confirmedPassword ) {
        log.debug("Request to save AppUser : {}", AppUser.class);
        AppUser appUser = appUserRepository.findByUsername(email) ;
        if (appUser != null) throw  new  RuntimeException("User Already exist !");
        if (!password.equals(confirmedPassword)) throw  new RuntimeException("Please confirm new passWord");
        AppUser appUser1 = new Customer();
        appUser1.setUsername(email);
        appUser1.setActivated(true);
        appUser1.setPassword(bCryptPasswordEncoder.encode(password));
        appUserRepository.save(appUser1);
        addRoleToUser(email, DEFAULT_ROLE);
        return appUser1;
    }


    @Override
    public AppUser loadUserByUsername(String email) {
        log.debug("Request to load AppUser : {}", email);
        return appUserRepository.findByUsername(email);
    }

    @Override
    public void addRoleToUser(String email, String rolename) {
        AppUser appUser = appUserRepository.findByUsername(email);
        AppRole appRole = appRoleRepository.findByRoleName(rolename);
        appUser.getAppUserRole().add(appRole);
    }

}
