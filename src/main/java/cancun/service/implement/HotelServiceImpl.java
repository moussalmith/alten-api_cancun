package cancun.service.implement;

import cancun.domain.Hotel;
import cancun.repository.HotelRepository;
import cancun.service.HotelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for mapping {@link Hotel}
 */
@Service
public class HotelServiceImpl implements HotelService {
    private final Logger log = LoggerFactory.getLogger(Hotel.class);

    private final HotelRepository hotelRepository;

    public HotelServiceImpl(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    @Override
    public Hotel save(Hotel hotel) {
        log.debug(" Request to save Hotel : {}", hotel);
        return hotelRepository.save(hotel);
    }

    @Override
    public Page<Hotel> findAll(Pageable pageable) {
        log.debug("Request to get all Hotels");
        return hotelRepository.findAll(pageable);
    }

    @Override
    public Optional<Hotel> findOne(String idHotel) {
        log.debug("Request to get Hotel : {}", idHotel);
        return hotelRepository.findById(idHotel);
    }

    @Override
    public void delete(String idHotel) {
        log.debug("Request to delete Hotel : {}", idHotel);
        hotelRepository.deleteById(idHotel);
    }

    @Override
    public Page<Hotel> search(String query, Pageable pageable) {
        log.debug("Request to search  for a page of Hotel for query {}", query);
        return null;
    }
}
