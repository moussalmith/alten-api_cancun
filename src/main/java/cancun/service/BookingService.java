package cancun.service;

import cancun.domain.Booking;
import javassist.tools.rmi.ObjectNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.util.Optional;

/**
 * Service Interface for managing {@link Booking}
 */
public interface BookingService {

    /**
     * Save a booking.
     *
     * @param booking the entity to save.
     * @return the persisted entity.
     */
    Booking save(Booking booking) throws ObjectNotFoundException;

    /**
     * Get all the booking.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Booking> findAll(Pageable pageable);

    /**
     * Get the "idBooking" booking.
     *
     * @param idBooking the id of the entity.
     * @return the entity.
     */
    Optional<Booking> findOne(String idBooking);

    /**
     * Delete the "idBooking" booking.
     *
     * @param idBooking the id of the entity.
     */
    void delete(String idBooking);

    /**
     * Search for the booking corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Booking> search(String query, Pageable pageable);

    /**
     *
     * @param instant1
     * @param instant2
     * @return the number of days
     */
    long getNumberDay(Instant instant1, Instant instant2);
}
