package cancun.service;

import cancun.domain.AppUser;
import org.springframework.security.core.Authentication;

import java.util.Optional;

/**
 * Service Interface for managing {@link AppUser}
 */
public interface AccountService {

    /**
     *
     *  Save a user
     * @param username
     * @param password
     * @param confirmedPassword
     * @return
     */
    AppUser saveUser(String username, String password, String confirmedPassword);

    /**
     * Load a appUser.
     *
     * @param username the entity to save.
     * @return the persisted entity.
     */
    AppUser loadUserByUsername(String username);

    /**
     * Add a Role App to a User by her username
     *
     * @param username
     * @param rolename
     */
    void addRoleToUser(String username, String rolename);
}

