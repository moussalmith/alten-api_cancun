package cancun.service;

import cancun.domain.Hotel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Hotel}
 */
public interface HotelService {

    /**
     * Save a hotel.
     *
     * @param hotel the entity to save.
     * @return the persisted entity.
     */
    Hotel save(Hotel hotel);

    /**
     * Get all the hotel.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Hotel> findAll(Pageable pageable);

    /**
     * Get the "idHotel" hotel.
     *
     * @param idHotel the id of the entity.
     * @return the entity.
     */
    Optional<Hotel> findOne(String idHotel);

    /**
     * Delete the "idHotel" hotel.
     *
     * @param idHotel the id of the entity.
     */
    void delete(String idHotel);

    /**
     * Search for the hotel corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Hotel> search(String query, Pageable pageable);
}
