package cancun.service;

import cancun.domain.AppUser;
import cancun.domain.Customer;
import cancun.domain.Employer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Customer}
 */
public interface AppUserService {

    /**
     * Save a customer.
     *
     * @param customer the entity to save.
     * @return the persisted entity.
     */
    Customer save(Customer customer);

    /**
     * Save a customer.
     *
     * @param employer the entity to save.
     * @return the persisted entity.
     */
    AppUser save(Employer employer);

    /**
     * Get all the customer.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AppUser> findAll(Pageable pageable);

    /**
     * Get the "idCustomer" customer.
     *
     * @param idCustomer the id of the entity.
     * @return the entity.
     */
    Optional<AppUser> findOne(String idCustomer);

    /**
     * Delete the "idCustomer" customer.
     *
     * @param idCustomer the id of the entity.
     */
    void delete(String idCustomer);

    /**
     * Search for the customer corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Customer> search(String query, Pageable pageable);
}
