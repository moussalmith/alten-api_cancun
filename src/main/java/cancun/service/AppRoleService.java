package cancun.service;

import cancun.domain.AppRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;


/**
 * Service Interface for managing {@link AppRole}
 */
public interface AppRoleService {

    /**
     * Save a appRole.
     *
     * @param appRole the entity to save.
     * @return the persisted entity.
     */
    AppRole save(AppRole appRole);

    /**
     * Get  the appRole.
     *
     * @param roleName the pagination information.
     * @return the  Approle of entities.
     */
    AppRole findByRoleName(String roleName);

    /**
     * Get all the appRole.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AppRole> findAll(Pageable pageable);

    /**
     * Get the "idAppRole" appRole.
     *
     * @param idAppRole the id of the entity.
     * @return the entity.
     */
    Optional<AppRole> findOne(String idAppRole);

    /**
     * Delete the "idAppRole" appRole.
     *
     * @param idAppRole the id of the entity.
     */
    void delete(String idAppRole);

    /**
     * Search for the appRole corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AppRole> search(String query, Pageable pageable);
}
