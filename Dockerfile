# Start with a base image containing Java runtime
FROM penjdk:11-slim

# Add Maintainer Info
LABEL maintainer="moussalmith@gmail.com"

# Build-time metadata as defined at http://label-schema.org
ARG BUILD_DATE
ARG VCS_REF
LABEL org.label-schema.build-date=$BUILD_DATE \
          org.label-schema.name="Api can services" \
          org.label-schema.description="API for a typical booking application . " \
          org.label-schema.url="https://gitlab.com/moussalmith/alten-api_cancun" \
          org.label-schema.vcs-ref=$VCS_REF \
          org.label-schema.vcs-url="https://gitlab.com/moussalmith/alten-api_cancun" \
          org.label-schema.version="latest" \
          org.label-schema.schema-version="latest"

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8080 available to the world outside this container
EXPOSE 8080

# The application's jar file
ARG JAR_FILE=target/api-cancun-latest.jar

# Add the application's jar to the container
ADD ${JAR_FILE} api-cancun-latest.jar

# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/api-cancun-latest.jar"]
